#include <iostream>
#include <cmath>

using namespace std;

bool esPrimo(const int* numero) {
    if (*numero <= 1)
        return false;
    if (*numero <= 3)
        return true;
    if (*numero % 2 == 0 || *numero % 3 == 0)
        return false;
    
    int divisor = 5;
    while (divisor * divisor <= *numero) {
        if (*numero % divisor == 0 || *numero % (divisor + 2) == 0)
            return false;
        divisor += 6;
    }
    return true;
}

void mostrarPrimos(const int* N) {
    cout << "Los primeros " << *N << " numeros primos son:\n";
    int contador = 0;
    int numero = 2;
    while (contador < *N) {
        if (esPrimo(&numero)) {
            cout << numero << " ";
            contador++;
        }
        numero++;
    }
    cout << endl;
}

int main() {
    int N;
    cout << "Ingrese la cantidad de numeros primos que desea mostrar: ";
    cin >> N;

    mostrarPrimos(&N);

    system("pause");

    return 0;
}